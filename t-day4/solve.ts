import { expect } from 'earl';
import * as fs from 'fs';
import path from 'path';

class Card {
    id: number;
    gagnants: Map<number, boolean>;
    selectionnees: number[];

    // Format de la string Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
    constructor(gameString: string) {
        const split = gameString.split(':');
        this.id = parseInt(split[0].split(" ")[1]);
        const tirage = split[1].split("|")
        const gagnantsString = tirage[0];
        const selectionneesString = tirage[1];

        this.gagnants = this.createMap(gagnantsString);
        this.selectionnees = this.createList(selectionneesString);
    }

    createList(selectionneesString: string): number[] {
        return selectionneesString.trim().split(" ")
            .map(n => parseInt(n.trim()))
            .filter(n => !isNaN(n));
    }
    createMap(gagnantsString: string): Map<number, boolean> {
        const map = new Map<number, boolean>();
        this.createList(gagnantsString).forEach(n => map.set(n, true));
        return map;
    }

    nombreNumeroIdentique() {
        return this.selectionnees.filter(n => this.gagnants.has(n)).length;
    }

    resultat() {
        const nombreEnCommun = this.nombreNumeroIdentique();
        return nombreEnCommun == 0 ? 0 : Math.pow(2, nombreEnCommun - 1);
    }
}

function solvePuzzle1(file: string) {
    const puzzle: string = fs.readFileSync(path.resolve(__dirname, file), 'utf8');
    return puzzle.split('\n')
        .map(line => new Card(line))
        .map(game => game.resultat())
        .reduce(sum);
}

function sum(prev: number, current: number, i_: number, a_: number[]) { return prev + current }
function solvePuzzle2(file: string) {
    const puzzle: string = fs.readFileSync(path.resolve(__dirname, file), 'utf8');

    const resultatCards = puzzle.split('\n')
        .map(line => new Card(line).nombreNumeroIdentique());

    const nbCards = Array.from(Array(resultatCards.length).keys()).map(_ => 1);
    for (let indexCard = 0; indexCard < resultatCards.length; indexCard++) {
        const resultatCard = resultatCards[indexCard];
        const nbCard = nbCards[indexCard];
        for (let j = 0; j < resultatCard; j++) {
            const indexNextCard = indexCard + j + 1;
            nbCards[indexNextCard] = nbCards[indexNextCard] + nbCard;
        }
    }

    return nbCards.reduce(sum);
}

const PUZZLE_TEST_FILE = "puzzle test";
const PUZZLE_FILE = "puzzle";

//Puzzle1
expect(solvePuzzle1(PUZZLE_TEST_FILE)).toEqual(13)
console.log("Resultat puzzle 1:", solvePuzzle1(PUZZLE_FILE)); // reponse correct: 32609

//Puzzle2
expect(solvePuzzle2(PUZZLE_TEST_FILE)).toEqual(30)
console.log("Resultat puzzle 2:", solvePuzzle2(PUZZLE_FILE));// reponse correct: 14624680