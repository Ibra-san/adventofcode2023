import { expect } from 'earl';
import * as fs from 'fs';
import path from 'path';
import { Utils } from '../utils/utils';


 class Hand {
    cards: string;
    bid: number;
    mapGroupeByType: Map<string, number>;
    power: number;

    static readonly orderPower = ["A", "K", "Q", "J", "T", "9", "8", "7", "6", "5", "4", "3", "2"].reverse();

    constructor(cardAndBid: string) {
        this.cards = cardAndBid.split(" ")[0].trim();
        this.bid = parseInt(cardAndBid.split(" ")[1].trim());
        this.mapGroupeByType = this.groupeByType(this.cards);
        this.power = this.calculatePower()
    }

    calculatePower() {
        //five of kind
        if (this.containsDoublon([5]))
            return 7;
        if (this.containsDoublon([4]))
            return 6;
        if (this.containsDoublon([3, 2]))
            return 5;
        if (this.containsDoublon([3]))
            return 4;
        if (this.containsDoublon([2, 2]))
            return 3;
        if (this.containsDoublon([2]))
            return 2;
        // Hight card
        return 1;
    }


    getListNbByType() {
        return [...this.mapGroupeByType.values()];
    }

    containsDoublon(expectList: number[]): boolean {
        const doublonsList = this.getListNbByType();
        return doublonsList.sort().reverse().join().startsWith(expectList.sort().reverse().join())
    }

    compare(hand: Hand): number {
        const compar = this.power - hand.power;
        if (compar == 0) {
            return this.compareHightestCard(this.cards, hand.cards);
        }
        return compar;
    }

    
     groupeByType(cards: string): Map<string, number> {
        const groupeByType = new Map<string, number>();
        for (let card of cards) {
            groupeByType.set(card, 1 + (groupeByType.get(card) || 0))
        }

        return groupeByType;
    }

     compareHightestCard(cards: string, cards1: string): number {
        for (let i = 0; i < cards.length; i++) {
            const compar = Hand.orderPower.indexOf(cards.charAt(i)) - Hand.orderPower.indexOf(cards1.charAt(i));
            if (compar != 0) {
                return compar;
            }
        }
        return 0;
    }
}

class HandJoker extends Hand {

    static readonly orderPowerJoker = ["A", "K", "Q", "T", "9", "8", "7", "6", "5", "4", "3", "2", "J"].reverse();

    override groupeByType(cards: string): Map<string, number> {
        const groupeByType = new Map<string, number>();
        for (let card of cards) {
            groupeByType.set(card, 1 + (groupeByType.get(card) || 0))
        }

        if (groupeByType.size === 1 && groupeByType.has("J")) {
            return groupeByType;
        }

        const nbJoker = groupeByType.get("J") || 0;
        groupeByType.delete("J");

        const maxItem = [...groupeByType.entries()].reduce((prev, current, _i, _a) => prev[1] > current[1] ? prev : current);
        groupeByType.set(maxItem[0], maxItem[1] + nbJoker);
        return groupeByType;
    }

    override compareHightestCard(cards: string, cards1: string): number {
        for (let i = 0; i < cards.length; i++) {
            const compar = HandJoker.orderPowerJoker.indexOf(cards.charAt(i)) - HandJoker.orderPowerJoker.indexOf(cards1.charAt(i));
            if (compar != 0) {
                return compar;
            }
        }
        return 0;
    }
}

class Game {
    hands: Hand[];
    handsJoker: HandJoker[];

    constructor(gameString: string) {
        this.hands = gameString.split("\n")
            .map(line => new Hand(line.trim()))


        this.handsJoker = gameString.split("\n")
            .map(line => new HandJoker(line.trim()))
    }

    productBidOrder(): number {
        const tri = this.hands.sort((a, b) => a.compare(b));
        return tri.reduce(
            (prev, current, i, _array) => {
                return prev + (current.bid * (i + 1));
            }, 0);
    }

    productBidOrderJoker(): number {
        const tri = this.handsJoker.sort((a, b) => a.compare(b));
        return tri.reduce(
            (prev, current, i, _array) => {
                return prev + (current.bid * (i + 1));
            }, 0);
    }

}

function solvePuzzle1(file: string) {
    const puzzle: string = fs.readFileSync(path.resolve(__dirname, file), 'utf8');
    return new Game(puzzle).productBidOrder();
}

function solvePuzzle2(file: string) {
    const puzzle: string = fs.readFileSync(path.resolve(__dirname, file), 'utf8');
    return new Game(puzzle).productBidOrderJoker();
}

const PUZZLE_TEST_FILE = "puzzle test";
const PUZZLE_FILE = "puzzle";

//Puzzle1
expect(solvePuzzle1(PUZZLE_TEST_FILE)).toEqual(6440)
Utils.time(() => console.log("Resultat puzzle 1:", solvePuzzle1(PUZZLE_FILE))); // reponse correct: 248812215

//Puzzle2
expect(solvePuzzle2(PUZZLE_TEST_FILE)).toEqual(5905)
Utils.time(() => console.log("Resultat puzzle 2: %d", solvePuzzle2(PUZZLE_FILE)));// reponse correct: 250057090