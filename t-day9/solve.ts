import { expect } from 'earl';
import * as fs from 'fs';
import path from 'path';
import { Utils } from '../utils/utils';

class Hystory {

    origineElements: number[];

    constructor(gameString: string) {
        this.origineElements = Utils.createListOfNumber(gameString);
    }

    predictNextElement(previous: boolean): number {
        const currentElements = [...this.origineElements];
        return this.predictNextElementRecurcif(currentElements, previous);
    }

    predictNextElementRecurcif(elements: number[], previous: boolean): number {
        const nextElements: number[] = [];
        for (let i = 0; i < elements.length - 1; i++) {
            nextElements.push(elements[i + 1] - elements[i]);
        }

        const lastValue = elements[elements.length - 1]; //A
        const firstValue = elements[0]; //B

        if (this.allZero(nextElements)) {
            return previous ? firstValue : lastValue;
        }

        const nextLineElement = this.predictNextElementRecurcif(nextElements, previous); //C


        /**
         * A  B
         *   C
         * 
         * B-A=C
         * B=C+A
         * A=-C+B => A=B-C
         */

        return previous ? firstValue - nextLineElement  : lastValue + nextLineElement;
    }

    allZero(elements: number[]) {
        return elements.every(e => e === 0)
    }


}

class EnvironnentOasis {

    hystory: Hystory[];

    constructor(gameString: string) {
        this.hystory = gameString.split("\n").map(line => new Hystory(line));
    }

    sumNextPrediction(): number {
        return this.hystory.map(h => h.predictNextElement(false)).reduce(Utils.reduceSum);
    }

    sumPredictionPrevious(): number {
        return this.hystory.map(h => h.predictNextElement(true)).reduce(Utils.reduceSum);
    }
}


function solvePuzzle1(file: string) {
    const puzzle: string = fs.readFileSync(path.resolve(__dirname, file), 'utf8');
    return new EnvironnentOasis(puzzle).sumNextPrediction();
}

function solvePuzzle2(file: string) {
    const puzzle: string = fs.readFileSync(path.resolve(__dirname, file), 'utf8');
    return new EnvironnentOasis(puzzle).sumPredictionPrevious();
}

const PUZZLE_TEST_FILE = "puzzle test";
const PUZZLE_FILE = "puzzle";

//Puzzle1
expect(solvePuzzle1(PUZZLE_TEST_FILE)).toEqual(114)
Utils.time(() => console.log("Resultat puzzle 1:", solvePuzzle1(PUZZLE_FILE))); // reponse correct: 1581679977

//Puzzle2
expect(solvePuzzle2(PUZZLE_TEST_FILE)).toEqual(2)
Utils.time(() => console.log("Resultat puzzle 2: %d", solvePuzzle2(PUZZLE_FILE)));// reponse correct: 889