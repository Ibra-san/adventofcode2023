import { expect } from 'earl';
import * as fs from 'fs';
import path from 'path';
import { Utils } from '../utils/utils';


class Race {
    distanceRecord: number;
    time: number;

    constructor(distance: number, time: number) {
        this.distanceRecord = distance;
        this.time = time;
    }

    possibleToWin(): number {
        return Utils.createArrayOfRange(1, this.time).filter(hold => {
            return this.distance(hold) > this.distanceRecord;
        }).length;
    }

    distance(holdTime: number): number {
        const tempsRestant = this.time - holdTime;
        return tempsRestant * holdTime;
    }
}
class Races {
    races: Race[];
    bigRace: Race;

    constructor(gameString: string) {
        const lines = gameString.split("\n");
        const times = Utils.createListOfNumber(lines[0]);
        const distances = Utils.createListOfNumber(lines[1]);
        this.races = Utils.createArrayOfRange(0, times.length).map(i => {
            return new Race(distances[i], times[i]);
        })

        this.bigRace = new Race(parseInt(distances.map(n => "" + n).join("")),
            parseInt(times.map(n => "" + n).join("")));
    }

    productPossibilityPerRace(): number {
        return this.races.map(r => r.possibleToWin()).reduce(Utils.reduceMultiply);
    }


    productPossibilityBigRace(): number {
        return this.bigRace.possibleToWin();
    }
}

function solvePuzzle1(file: string) {
    const puzzle: string = fs.readFileSync(path.resolve(__dirname, file), 'utf8');
    return new Races(puzzle).productPossibilityPerRace();
}

function solvePuzzle2(file: string) {
    const puzzle: string = fs.readFileSync(path.resolve(__dirname, file), 'utf8');
    return new Races(puzzle).productPossibilityBigRace();
}

const PUZZLE_TEST_FILE = "puzzle test";
const PUZZLE_FILE = "puzzle";

//Puzzle1
expect(solvePuzzle1(PUZZLE_TEST_FILE)).toEqual(288)
Utils.time(() => console.log("Resultat puzzle 1:", solvePuzzle1(PUZZLE_FILE))); // reponse correct: 303600

//Puzzle2
expect(solvePuzzle2(PUZZLE_TEST_FILE)).toEqual(71503)
Utils.time(() => console.log("Resultat puzzle 2: %d", solvePuzzle2(PUZZLE_FILE)));// reponse correct: 23654842 en 3 secondes 