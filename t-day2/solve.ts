import { expect } from 'earl';
import * as fs from 'fs';
import path from 'path';

interface Set {
    red: number,
    blue: number,
    green: number
}
class Game {
    id: number;
    sets: Set[];

    // Format de la string Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
    constructor(gameString: string) {
        const split = gameString.split(':');
        this.id = parseInt(split[0].split(" ")[1]);
        this.sets = split[1].split(";").map(
            setString => {
                let red = 0, green = 0, blue = 0;
                for (const draw of setString.split(",")) {
                    const nb = parseInt(draw.trim().split(" ")[0].trim());
                    const color = draw.trim().split(" ")[1].trim();
                    if (color.startsWith("red")) {
                        red = nb;
                    } else if (color.startsWith("green")) {
                        green = nb;
                    } else if (color.startsWith("blue")) {
                        blue = nb;
                    }
                }
                return { red: red, green: green, blue: blue }
            }
        )
    }

    isPossible({ red, blue, green }: Set): boolean {
        return this.sets.every(s => s.red <= red && s.green <= green && s.blue <= blue);
    }

    power(): number {
        const fewer = this.fewerSetPossible();
        return fewer.red * fewer.green * fewer.blue;
    }

    fewerSetPossible() {
        return this.sets.reduce(
            (prev, current, _i, _a) => {
                return { red: Math.max(prev.red, current.red), green: Math.max(prev.green, current.green), blue: Math.max(prev.blue, current.blue) }
            });
    }
}

function solvePuzzle1(file: string, set: Set) {
    const puzzle: string = fs.readFileSync(path.resolve(__dirname, file), 'utf8');
    return puzzle.split('\n')
        .map(line => new Game(line))
        .filter(game => game.isPossible(set))
        .map(game => game.id)
        .reduce((prev, current, i_, a_) => prev + current, 0);
}

function solvePuzzle2(file: string) {
    const puzzle: string = fs.readFileSync(path.resolve(__dirname, file), 'utf8');
    return puzzle.split('\n')
        .map(line => new Game(line).power())
        .reduce((prev, current, i_, a_) => prev + current, 0);
}

const PUZZLE_TEST_FILE = "puzzle test";
const PUZZLE_FILE = "puzzle";

//Puzzle1
expect(solvePuzzle1(PUZZLE_TEST_FILE, { red: 12, green: 13, blue: 14 })).toEqual(8)
console.log("Resultat puzzle 1:", solvePuzzle1(PUZZLE_FILE, { red: 12, green: 13, blue: 14 })); // reponse correct: 2720

//Puzzle2
expect(solvePuzzle2(PUZZLE_TEST_FILE)).toEqual(2286)
console.log("Resultat puzzle 2:", solvePuzzle2(PUZZLE_FILE));
