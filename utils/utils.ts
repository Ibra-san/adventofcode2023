import moment from "moment";

export class Utils {
    static createArrayOfRange(begin: number, end: number) {
        return Array.from(new Array(end - begin).keys()).map(t => t + begin);
    }

    static isNumber(value: string) {
        return value >= '0' && value <= '9';
    }

    static reduceMin(prev: number, current: number, i_: number, a_: number[]) { return prev < current ? prev : current }
    static reduceMax(prev: number, current: number, i_: number, a_: number[]) { return prev > current ? prev : current }
    static reduceSum(prev: number, current: number, i_: number, a_: number[]) { return prev + current }
    static reduceMultiply(prev: number, current: number, i_: number, a_: number[]) { return prev * current }

    static createListOfNumber(line: string): number[] {
        return line.trim().split(" ")
            .map(nb => nb.trim())
            .map(p => parseInt(p))
            .filter(nb => !isNaN(nb));
    }

    static time(funct: () => void) {
        const start = moment();
        funct.apply(null);
        const diff = moment().diff(start, 'second');
        console.log("Resolue en %d secondes", diff);
    }

}