import { expect } from 'earl';
import * as fs from 'fs';
import path from 'path';
import { Utils } from '../utils/utils';


class Noeud {
    id: string;
    left: string;
    right: string;
    memoryDestination: Noeud | undefined;

    // format AAA = (BBB, CCC)
    constructor(line: string) {
        const split = line.split("=");
        this.id = split[0].trim();
        const splitDirection = split[1].split(",");
        this.left = splitDirection[0].trim().substring(1);
        this.right = splitDirection[1].trim().substring(0, splitDirection[1].trim().length - 1);
    }
    goTo(d: string) {
        return d === "L" ? this.left : this.right;
    }

    isFinal() {
        return this.id.endsWith("Z");
    }
    startWithA() {
        return this.id.endsWith("A");
    }
}

class Labyrinth {

    intruction: string;
    lab: Map<string, Noeud>;

    constructor(gameString: string) {
        const lines = gameString.split("\n");
        this.intruction = (lines.shift() || "").trim();
        const lab = new Map<string, Noeud>();
        lines
            .filter(noeudString => noeudString.trim().length !== 0)
            .map(line => new Noeud(line))
            .forEach(noeud => lab.set(noeud.id, noeud))
        this.lab = lab;
    }

    getNode(idNoeud: string): Noeud {
        let currentNode = this.lab.get(idNoeud);
        if (!currentNode)
            throw new Error("pas de noeud " + idNoeud + " existant");
        return currentNode;
    }


    getAllNodeStartWithA(): Noeud[] {
        return [...this.lab.values()].filter(n => n.startWithA());
    }

    nbRepeatToExit(): number {
        let nbTour = 0;
        let currentNode = this.getNode("AAA");
        while (!currentNode.isFinal()) {
            currentNode = this.followInstruction(currentNode);
            nbTour++;
        }
        return nbTour * this.intruction.length;
    }

    nbRepeatToExitAllNode(): number {
        let nbTour = 0;
        let currentNodes = this.getAllNodeStartWithA();
        while (!currentNodes.every((noeud, _i, _a) => noeud.isFinal())) {
            currentNodes = currentNodes.map(c => this.followInstruction(c));
            // const leaf = currentNodes.filter(n => n.memoryDestination).length;
            // console.log(leaf, "/", currentNodes.length)
            // console.log(currentNodes.map(c=>c.id))
            // if (leaf === currentNodes.length)
            //     throw new Error("Boucle infini");
            nbTour++;
        }
        return nbTour * this.intruction.length;
    }


    followInstruction(startNode: Noeud): Noeud {
        if (startNode.memoryDestination) {
            return startNode.memoryDestination;
        }
        let currentNode = startNode;
        [...this.intruction].forEach(direction => currentNode = this.getNode(currentNode.goTo(direction)));
        startNode.memoryDestination = currentNode;
        return currentNode;
    }
}


function solvePuzzle1(file: string) {
    const puzzle: string = fs.readFileSync(path.resolve(__dirname, file), 'utf8');
    return new Labyrinth(puzzle).nbRepeatToExit();
}

function solvePuzzle2(file: string) {
    const puzzle: string = fs.readFileSync(path.resolve(__dirname, file), 'utf8');
    return new Labyrinth(puzzle).nbRepeatToExitAllNode();
}

const PUZZLE_TEST_FILE = "puzzle test";
const PUZZLE_TEST_FILE_2 = "puzzle test 2";
const PUZZLE_FILE = "puzzle";

//Puzzle1
expect(solvePuzzle1(PUZZLE_TEST_FILE)).toEqual(2)
expect(solvePuzzle1(PUZZLE_TEST_FILE_2)).toEqual(6)
Utils.time(() => console.log("Resultat puzzle 1:", solvePuzzle1(PUZZLE_FILE))); // reponse correct: 17287

//Puzzle2
const PUZZLE_2_TEST_FILE = "puzzle 2 test";
expect(solvePuzzle2(PUZZLE_2_TEST_FILE)).toEqual(6)
Utils.time(() => console.log("Resultat puzzle 2: %d", solvePuzzle2(PUZZLE_FILE)));// reponse correct: 17287 // reponse incorrect