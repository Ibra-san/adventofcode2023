import { expect } from 'earl';
import * as fs from 'fs';
import path from 'path';

function estChiffre(value: string): boolean { return value >= '0' && value <= '9' };
function getChiffreEnLettre(line: string): string | null {
    if (line.startsWith("one")) {
        return '1'
    } else if (line.startsWith("two")) {
        return '2'
    } if (line.startsWith("three")) {
        return '3'
    } if (line.startsWith("four")) {
        return '4'
    } if (line.startsWith("five")) {
        return '5'
    } if (line.startsWith("six")) {
        return '6'
    } if (line.startsWith("seven")) {
        return '7'
    } if (line.startsWith("eight")) {
        return '8'
    } if (line.startsWith("nine")) {
        return '9'
    }
    return null;
};

function extractChiffre(line: string): string | null {
    let firstCharatere = line.charAt(0);
    if (estChiffre(firstCharatere)) {
        return firstCharatere;
    }
    return getChiffreEnLettre(line);
}

function extractNumber(line: string): number {
    let first: string = '';
    let last: string = '';
    for (let i = 0; i < line.length; i++) {
        let endLine = line.substring(i);
        let chiffre = extractChiffre(endLine);
        if (chiffre) {
            if (first === '')
                first = chiffre;
            last = chiffre;
        }
    }
    return parseInt(first + last);
}


function solvePuzzle(file: string) {
    const puzzle: string = fs.readFileSync(path.resolve(__dirname, file), 'utf8');
    return puzzle.split('\n')
        .reduceRight((pre, current, i, array) => pre + extractNumber(current), 0);
}

const PUZZLE_TEST_FILE = "puzzle test";
const PUZZLE_FILE = "puzzle";

expect(solvePuzzle(PUZZLE_TEST_FILE)).toEqual(281)
console.log(solvePuzzle(PUZZLE_FILE)); // reponse 55218

