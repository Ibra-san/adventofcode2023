import { expect } from 'earl';
import * as fs from 'fs';
import path from 'path';

interface Schematic {
    indexRow: number,
    indexColStart: number,
    indexColEnd: number
}
class Engine {

    rawEngine: string[];
    schematics: Schematic[];
    etoiles: { rowIndex: number, colIndex: number }[];

    constructor(puzzle: string) {
        this.rawEngine = puzzle.split("\n").map(r => r.trim());
        this.schematics = [];
        this.etoiles = [];
        for (let indexRow = 0; indexRow < this.rawEngine[0].length; indexRow++) {
            const row = this.rawEngine[indexRow];
            let startNumberCol: number | null = null;
            for (let indexColumn = 0; indexColumn < row.length; indexColumn++) {
                const colValue = row.charAt(indexColumn);
                if (this.estUnEtoile(colValue)) {
                    this.etoiles.push({ rowIndex: indexRow, colIndex: indexColumn });
                }

                if (this.estUnChiffre(colValue)) {
                    if (startNumberCol === null) {
                        startNumberCol = indexColumn;
                    }
                } else {
                    if (startNumberCol !== null) {
                        this.schematics.push({ indexRow: indexRow, indexColStart: startNumberCol, indexColEnd: indexColumn - 1 })
                        startNumberCol = null;
                    }

                }
            }

            if (startNumberCol !== null) {
                this.schematics.push({ indexRow: indexRow, indexColStart: startNumberCol, indexColEnd: row.length - 1 })
                startNumberCol = null;
            }
        }

    }

    estUnChiffre(v: string): boolean {
        return v >= '0' && v <= '9';
    }
    estUnEtoile(v: string): boolean {
        return v === '*';
    }

    valueOfSchematic(schematic: Schematic) {
        const row = this.rawEngine[schematic.indexRow];
        return parseInt(row.substring(schematic.indexColStart, schematic.indexColEnd + 1));
    }


    aUnSymbolEnVoisin(schematic: Schematic): boolean {
        for (let row = schematic.indexRow - 1; row <= schematic.indexRow + 1; row++) {
            for (let col = schematic.indexColStart - 1; col <= schematic.indexColEnd + 1; col++) {
                if (this.estSymbol(row, col)) {
                    return true;
                }
            }
        }
        return false;
    }
    estSymbol(rowIndex: number, colIndex: number): boolean {
        if (rowIndex < 0 || rowIndex >= this.rawEngine.length) {
            return false;
        }
        const row = this.rawEngine[rowIndex];
        if (colIndex < 0 || colIndex >= row.length) {
            return false;
        }
        const colValue = row.charAt(colIndex);
        return !this.estUnChiffre(colValue) && this.nEstPasUnPoint(colValue);
    }

    private nEstPasUnPoint(colValue: string): boolean {
        return colValue !== '.';
    }

    allPartEngineSchematic(): number {
        return this.schematics
            .filter(sch => this.aUnSymbolEnVoisin(sch))
            .map(sch => this.valueOfSchematic(sch))
            .reduce(sum)
    }

    ratioEngine(): number {
        return this.etoiles.map(etoile => {
            const schVoisin = this.schematics.filter(
                sch => this.sontVoisin(etoile, sch)
            )

            if (schVoisin.length == 2) {
                return this.valueOfSchematic(schVoisin[0]) * this.valueOfSchematic(schVoisin[1]);
            }
            return 0;
        }).reduce(sum)
    }

    private sontVoisin(etoile: { rowIndex: number; colIndex: number; }, sch: Schematic): boolean {
        return (etoile.rowIndex >= sch.indexRow - 1 && etoile.rowIndex <= sch.indexRow + 1)
            && (etoile.colIndex >= sch.indexColStart - 1 && etoile.colIndex <= sch.indexColEnd + 1);
    }
}


function sum(prev: number, current: number, _i: number, _a: number[]) { return prev + current }
function solvePuzzle1(file: string) {
    const puzzle: string = fs.readFileSync(path.resolve(__dirname, file), 'utf8');
    return new Engine(puzzle).allPartEngineSchematic();
}

function solvePuzzle2(file: string) {
    const puzzle: string = fs.readFileSync(path.resolve(__dirname, file), 'utf8');
    return new Engine(puzzle).ratioEngine();
}

const PUZZLE_TEST_FILE = "puzzle test";
const PUZZLE_FILE = "puzzle";

//Puzzle1
expect(solvePuzzle1(PUZZLE_TEST_FILE)).toEqual(4361)
console.log("Resultat puzzle 1:", solvePuzzle1(PUZZLE_FILE)); // reponse correct: 556057

//Puzzle2

expect(solvePuzzle2(PUZZLE_TEST_FILE)).toEqual(467835)
console.log("Resultat puzzle 2:", solvePuzzle2(PUZZLE_FILE)); // reponse correct: 82824352
