import { expect } from 'earl';
import * as fs from 'fs';
import path from 'path';
import { Utils } from '../utils/utils';

class Transition {
    sourceStart: number;
    destinationStart: number;
    sourceEnd: number;
    offset: number;

    constructor(
        sourceStart: number,
        destinationStart: number,
        range: number) {
        this.sourceStart = sourceStart;
        this.sourceEnd = this.sourceStart + range - 1;
        this.destinationStart = destinationStart;
        this.offset = this.destinationStart - this.sourceStart;
    }

    estDansSourceRange(value: number): boolean {
        return value >= this.sourceStart && value <= this.sourceEnd;
    }

    makeTransition(value: number): number {
        return value + (this.offset)
    }
}

interface SeedRange {
    readonly start: number,
    readonly range: number
}

class TransitionMap {

    transitions: Transition[];
    minSource: number;
    maxSource: number;
    /**
     * Format line 
     * 0 15 37
     * 37 52 2
     * 39 0 15
     */
    constructor(lines: string[]) {
        this.transitions = lines.map(line => {
            const numbers = line.split(" ")
                .map(nb => nb.trim())
                .map(p => parseInt(p))
                .filter(nb => !isNaN(nb));
            const sourceStart = numbers[1];
            const destinationStart = numbers[0];
            const range = numbers[2];

            return new Transition(sourceStart, destinationStart, range);
        });
        this.minSource = this.transitions.map(t => t.sourceStart).reduce(Utils.reduceMin);
        this.maxSource = this.transitions.map(t => t.sourceEnd).reduce(Utils.reduceMax);
    }

    get(value: number): number {
        if (this.peutAvoirUneTransition(value)) {
            for (let transition of this.transitions) {
                if (transition.estDansSourceRange(value)) {
                    return transition.makeTransition(value);
                }
            }
        }
        return value;
    }

    peutAvoirUneTransition(value: number): boolean {
        return value >= this.minSource && value <= this.maxSource;
    }


}

class Almanac {

    seedToSoil: TransitionMap;
    soilToFertilizer: TransitionMap;
    fertilizerToWater: TransitionMap;
    waterToLight: TransitionMap;
    lightToTemperature: TransitionMap;
    temperatureToHumidity: TransitionMap;
    humidityToLocation: TransitionMap;

    seeds: number[];

    constructor(gameString: string) {
        const lines = gameString.split("\n");
        //Seeds
        this.seeds = (lines.shift() || "").split(":")[1].split(" ").map(n => parseInt(n)).filter(n => !isNaN(n));
        let transitionLines: string[] = [];
        const transitions: TransitionMap[] = [];
        for (const line of lines) {
            if (line.trim() === '' && transitionLines.length > 0) {
                transitions.push(new TransitionMap(transitionLines));
                transitionLines = [];
            }
            if (Utils.isNumber(line.charAt(0))) {
                transitionLines.push(line);
            }
        }
        if (transitionLines.length > 0) {
            transitions.push(new TransitionMap(transitionLines));
            transitionLines = [];

        }
        this.seedToSoil = transitions[0];
        this.soilToFertilizer = transitions[1];
        this.fertilizerToWater = transitions[2];
        this.waterToLight = transitions[3];
        this.lightToTemperature = transitions[4];
        this.temperatureToHumidity = transitions[5];
        this.humidityToLocation = transitions[6];

    }

    minSeedLocalisationRange() {
        const r = this.createSeedRangeList().map(seedRange => {
            let minSeedLocalisation = Number.MAX_VALUE;
            for (let seed = seedRange.start; seed < seedRange.start + seedRange.range; seed++) {
                const seedLocalisation = this.minSeedLocalisation(seed);
                if (seedLocalisation < minSeedLocalisation) {
                    minSeedLocalisation = seedLocalisation;
                }
            }
            return minSeedLocalisation;
        }).reduce(Utils.reduceMin);

        return r;
    }

    createSeedRangeList(): SeedRange[] {
        const result: SeedRange[] = [];
        for (let i = 0; i < this.seeds.length; i = i + 2) {
            result.push({ start: this.seeds[i], range: this.seeds[i + 1] });
        }
        return result;
    }


    minSeedLocalisationOriginal(): number {
        return this.seeds.map(s => this.minSeedLocalisation(s))
            .reduce((prev, current, _i, _array) => Math.min(prev, current))
    }

    minSeedLocalisation(seed: number): number {
        let localisation = seed;
        this.getTransition()
            .forEach(transition => {
                localisation = transition.get(localisation);
            });
        return localisation;
    }

    getTransition(): TransitionMap[] {
        return [
            this.seedToSoil,
            this.soilToFertilizer,
            this.fertilizerToWater,
            this.waterToLight,
            this.lightToTemperature,
            this.temperatureToHumidity,
            this.humidityToLocation
        ]
    }



}

function solvePuzzle1(file: string) {
    const puzzle: string = fs.readFileSync(path.resolve(__dirname, file), 'utf8');
    return new Almanac(puzzle).minSeedLocalisationOriginal();
}

function solvePuzzle2(file: string) {
    const puzzle: string = fs.readFileSync(path.resolve(__dirname, file), 'utf8');
    return new Almanac(puzzle).minSeedLocalisationRange();
}

const PUZZLE_TEST_FILE = "puzzle test";
const PUZZLE_FILE = "puzzle";

//Puzzle1
expect(solvePuzzle1(PUZZLE_TEST_FILE)).toEqual(35)
Utils.time(() => console.log("Resultat puzzle 1:", solvePuzzle1(PUZZLE_FILE))); // reponse correct: 389056265

//Puzzle2
expect(solvePuzzle2(PUZZLE_TEST_FILE)).toEqual(46)
Utils.time(() => console.log("Resultat puzzle 2: %d", solvePuzzle2(PUZZLE_FILE)));// reponse correct: 137516820 en 327 secondes 